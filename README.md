# Study Abroad: MSc. in Computer Science and related fields
This list will be continuously updated

## Scholarships
* [DAAD](https://www.daad.org/en/)
* [BAYHOST](http://www.uni-r.de/bayhost/english/scholarships/study-in-bavaria/index.html) - One-year scholarship to study in Bavaria (can be extended)

## Programmes
* [Erasmus Mundus BDMA](http://bdma.univ-tours.fr/bdma/) - Joint Master Degree Programme in Big Data Management and Analytics
* [EMECS](http://www.emecs.eu/) - European Master in Embedded Computing Systems, Erasmus Mundus programme

## Only for Ukrainians
* [Fulbright Ukraine](http://www.fulbright.org.ua/)
* [Free Student in Slovakia](http://www.free-student-slovakia.org/ua/)

## Useful Links
* [Masters Portal](http://www.mastersportal.eu/) - Find & compare thousands of Masters, worldwide
* [Study in Germany](https://www.study-in.de/en/index.php)
* [Study programmes in UK](https://www.ucas.com/)
